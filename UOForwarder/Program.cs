﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using System.Security.Cryptography;

using Ultima;
using System.Web;
using System.Collections.Specialized;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Reflection;

namespace UOForwarder
{
    public class ForwaderServer
    {
        public static readonly string Title = "UOWS Forwarder (Version " + Assembly.GetExecutingAssembly().GetName().Version.ToString() + ", using Ultima.dll " + Assembly.GetAssembly(typeof(Ultima.Animations)).GetName().Version.ToString() + ")";
        static void Main(string[] args)
        {
            Console.Title = Title;
            Console.WriteLine(Title);
            Listen();
        }


        public class WebSocketClient
        {
            public class Packet
            {
                public byte[] Data;
                private byte[] m_Mask;
                private int m_BytesRead;
                public byte[] Mask
                {
                    get { return m_Mask; }
                    set { m_Mask = value; }
                }
                public int BytesRead
                {
                    get { return m_BytesRead; }
                    set { m_BytesRead = value; }
                }/*
            public byte[] Data
            {
                get { return m_Data; }
                set { m_Data = value; }
            }*/
            }
            public readonly int MaxBufferSize = 2048;
            private Socket m_WebSocket;
            private Socket m_UOClientSocket;
            private byte[] m_Buffer, m_UOBuffer;
            private bool m_SentHeaders, m_UOCompressed;
            private Packet m_CurrentPacket;
            private DateTime m_CreationTime;
            public bool Compressed
            {
                get { return m_UOCompressed; }
                set { m_UOCompressed = value; }
            }
            public DateTime CreationTime
            {
                get { return m_CreationTime; }
            }
            public Socket UOSocket
            {
                get { return m_UOClientSocket; }
                set { m_UOClientSocket = value; }
            }
            public Packet CurrentPacket
            {
                get { return m_CurrentPacket; }
                set { m_CurrentPacket = value; }
            }
            public bool SentHeaders
            {
                get { return m_SentHeaders; }
                set { m_SentHeaders = value; }
            }
            public Socket WebSocket
            {
                get { return m_WebSocket; }
            }
            public byte[] UOWriteBuffer
            {
                get { return m_UOBuffer == null ? (m_UOBuffer = new byte[MaxBufferSize]) : m_UOBuffer; }
                set { m_UOBuffer = value; }
            }
            public byte[] WriteBuffer
            {
                get { return m_Buffer == null ? (m_Buffer = new byte[MaxBufferSize]) : m_Buffer; }
                set { m_Buffer = value; }
            }
            public WebSocketClient(Socket webSocket)
            {
                m_WebSocket = webSocket;
                m_CreationTime = DateTime.Now;
            }

            public void Close()
            {
                if (m_WebSocket != null && m_WebSocket.Connected)
                {
                    Console.WriteLine("Client [{0}]: Closing", m_WebSocket.RemoteEndPoint);
                    m_WebSocket.Close();
                }
                else
                    Console.WriteLine("Client [d/c]: Closed");
                if (m_UOClientSocket != null && m_UOClientSocket.Connected)
                    m_UOClientSocket.Close();
            }
        }

        private static Socket m_ListenerSocket;
        private static bool m_Listening;
        private static ManualResetEvent m_AcceptEvent;
        private static DateTime m_StartDate;
        public enum RequestType
        {
            GameRequest,
            WebRequest
        }

        public enum PacketType
        {
            GamePacket = 'G',
            ClientPacket = 'L',
            None = 0x00
        }

        public static void Listen()
        {
            m_StartDate = DateTime.UtcNow;
            m_ListenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_Widths = new Dictionary<int, byte[]>();
            m_Listening = true;
            m_AcceptEvent = new ManualResetEvent(false);

            StartListen();
        }
        public static void StartListen()
        {
            //m_Listening = true;
            //try
            //{
            m_ListenerSocket.Bind(new IPEndPoint(IPAddress.Any, 2580));
            m_ListenerSocket.LingerState.Enabled = true;
            m_ListenerSocket.Listen(8);
            Console.WriteLine("Listener has started: {0}", m_ListenerSocket.LocalEndPoint);
            while (m_Listening)
            {
                //Console.WriteLine("Waiting for connections");
                m_AcceptEvent.Reset();
                m_ListenerSocket.BeginAccept(new AsyncCallback(AcceptCallback), m_ListenerSocket);
                m_AcceptEvent.WaitOne();
            }
            /* }
             catch (Exception e)
             {
                 Console.WriteLine("Exception thrown: {0}\r\n{1}", e, e.StackTrace);
                 m_Listening = false;
                 return;
             }*/
        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            m_AcceptEvent.Set();

            Socket webSocket = m_ListenerSocket.EndAccept(ar);
            WebSocketClient client = new WebSocketClient(webSocket);

            Console.WriteLine("Client [{0}]: Connection Detected", client.WebSocket.RemoteEndPoint);
            webSocket.BeginReceive(client.WriteBuffer, 0, client.WriteBuffer.Length, SocketFlags.None, new AsyncCallback(ReadCallback), client);
        }

        private static SHA1 m_SHA1;
        private static readonly string MagicKey = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        public static RequestType CreateHeaders(string header, out string sendHeaders, out string shortUri, out string fullUri)
        {
            string[] headers = header.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            string[] first = headers[0].Split(' ');
            string method = first[0];
            fullUri = first[1];
            shortUri = fullUri.Substring(0, fullUri.IndexOf('?') == -1 ? fullUri.Length : fullUri.IndexOf('?'));
            switch (shortUri)
            {
                default:
                    {
                        sendHeaders = string.Format("HTTP/1.1 200 OK{0}"
                            + "Content-Type: text/html{0}"
                            + "Connection: close{0}{0}<h1>Hello.</h1>This server is running {1}. The server time is {2}", "\r\n", Title, DateTime.Now);

                        return RequestType.WebRequest;
                    }
                case "/getcliloc":
                case "/getaniminfo":
                case "/getmapinfo":
                case "/td":
                    {
                        sendHeaders = string.Format("HTTP/1.1 200 OK{0}"
                            + "Content-Type: application/javascript{0}"
                            + "Access-Control-Allow-Origin: *{0}"
                            + "Connection: close{0}{0}", "\r\n");

                        return RequestType.WebRequest;
                    }
                
                case "/getgump":
                case "/getobj":
                case "/getanim":
                    {
                        sendHeaders = string.Format("HTTP/1.1 200 OK{0}"
                                           + "Content-Type: image/png{0}"
                                           + "Cache-Control: public, max-age=1209600{0}"
                                           + "Access-control-allow-origin: *{0}"
                                           + "Access-control-allow-credentials: true{0}"
                                           + "Expires: {1}{0}"
                                           + "Last-Modified: {2}{0}"
                                           + "Connection: close{0}{0}", "\r\n", (DateTime.Now + TimeSpan.FromDays(14)).ToString("r"), m_StartDate.ToString("r"));
                        return RequestType.WebRequest;
                    }
                case "/game":
                    {
                        if (m_SHA1 == null)
                            m_SHA1 = SHA1.Create();
                        string auth = null, orgin = null, host = null;

                        foreach (string h in headers)
                        {
                            string[] parts = h.Split(new char[] { ' ' }, 2);
                            switch (parts[0])
                            {
                                case "Sec-WebSocket-Key:":
                                    {
                                        //Console.WriteLine("'{0}'", parts[1]);
                                        auth = Convert.ToBase64String(m_SHA1.ComputeHash(Encoding.UTF8.GetBytes(parts[1] + MagicKey)));
                                        break;
                                    }
                                case "Orgin:":
                                    {
                                        orgin = parts[1];
                                        break;
                                    }
                                case "Host:":
                                    {
                                        host = parts[1];
                                        break;
                                    }
                            }
                            /*if (header.StartsWith("Sec-WebSocket-Key"))
                            {
                                string key = header.Split(' ')[1];
                                auth = Convert.ToBase64String(m_SHA1.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key + MagicKey)));
                            }*/
                        }
                        sendHeaders = string.Format("HTTP/1.1 101 Switching Protocols{0}"
                        + "Upgrade: WebSocket{0}"
                        + "Connection: Upgrade{0}"
                        + "Sec-WebSocket-Accept: {1}{0}"
                            //+ "Sec-WebSocket-Protocol: chat{0}"
                        + "WebSocket-Protocol: 13{0}"
                        + "Orgin: {2}{0}"
                        + "WebSocket-Location: ws://{3}/{4}{0}{0}", "\r\n", auth, orgin == null ? "http://127.0.0.1" : orgin, host, fullUri.Replace("/", ""));

                        return RequestType.GameRequest;
                    }
            }
        }
        public static void ReadCallback(IAsyncResult ar)
        {
            WebSocketClient client = (WebSocketClient)ar.AsyncState;
            Socket webSocket = client.WebSocket;
            int read;
            try
            {
                read = webSocket.Connected ? webSocket.EndReceive(ar) : 0;
            }
            catch (Exception e)
            {
                read = 0;
                Console.WriteLine("Socket [d/c]: {0}", e.Message);
            }
            if (read > 0)
            {
                if (!client.SentHeaders)
                {
                    string sendHeaders, shortUri, fullUri;
                    RequestType type = CreateHeaders(Encoding.ASCII.GetString(client.WriteBuffer), out sendHeaders, out shortUri, out fullUri);

                    switch (type)
                    {
                        case RequestType.GameRequest:
                            {
                                client.WebSocket.BeginSend(ASCIIEncoding.ASCII.GetBytes(sendHeaders), 0, sendHeaders.Length, SocketFlags.None, new AsyncCallback(SendCallback), client);
                                Console.WriteLine("Client [{0}]: Handshake OK", client.WebSocket.RemoteEndPoint);
                                break;
                            }
                        case RequestType.WebRequest:
                            {
                                Console.WriteLine("WebReq [{0}]: Parsed", client.WebSocket.RemoteEndPoint);
                                byte[] toSend = ParseWebRequest(client, shortUri, fullUri);
                                byte[] headers = ASCIIEncoding.ASCII.GetBytes(sendHeaders);
                                byte[] all = new byte[headers.Length + toSend.Length];


                                Array.Copy(headers, all, headers.Length);
                                Array.Copy(toSend, 0, all, headers.Length, toSend.Length);
                                
                                //client.WebSocket.Send(all);
                                client.WebSocket.BeginSend(all, 0, all.Length, SocketFlags.None, new AsyncCallback(WebSendCallback), client);
                                
                                return;
                            }
                    }
                    client.SentHeaders = true;
                }
                else if (client.CurrentPacket != null)
                {

                    //bool mismatch = false;
                    int length = client.CurrentPacket.Data.Length;

                    if (client.CurrentPacket.Data.Length < (client.CurrentPacket.BytesRead + read))
                    {
                        //mismatch = true;
                        Console.WriteLine("packet length mismatch (should be {0}, but is {1})", client.CurrentPacket.Data.Length, (client.CurrentPacket.BytesRead + read));

                        /*
                         what I want to do:
                         
                         create buffer "next" 
                         */
                        //byte[] next = new byte[client.CurrentPacket.BytesRead + read - client.CurrentPacket.Data.Length]
                        //Array.Copy(client.WriteBuffer,  
                        //Array.Copy(latter, 0, client.CurrentPacket.Data, 0, read);

                        Array.Resize<byte>(ref client.CurrentPacket.Data, client.CurrentPacket.BytesRead + read);
                        Array.Copy(client.WriteBuffer, 0, client.CurrentPacket.Data, client.CurrentPacket.BytesRead, read);

                        //Array.Copy(client.WriteBuffer, 0, client.CurrentPacket.Data, client.CurrentPacket.BytesRead, client.CurrentPacket.Data.Length - client.CurrentPacket.BytesRead);
                        //Array.Copy(client.WriteBuffer, 0, client.CurrentPacket.Data, client.CurrentPacket.BytesRead, read - client.CurrentPacket.Data.Length);
                    }
                    for (int i = client.CurrentPacket.BytesRead; i < client.CurrentPacket.BytesRead + read; i++)
                        client.CurrentPacket.Data[i] ^= client.CurrentPacket.Mask[i % 4];
                    //if (mismatch)
                        //PrintHexArray(client.CurrentPacket.Data, client.CurrentPacket.Data.Length);
                    client.CurrentPacket.BytesRead += read;

                    if (client.CurrentPacket.BytesRead >= client.CurrentPacket.Data.Length)
                    {
                        //Console.WriteLine("Match: {0} <{1}> {2} <>", client.CurrentPacket.Data.Length, client.CurrentPacket.BytesRead, read);
                        OnReceiveFromWebSocket(client, client.CurrentPacket.Data, length);
                        //Send(client, ASCIIEncoding.UTF8.GetBytes(string.Format("large packet done: thanks for {0} bytes!", client.CurrentPacket.Data.Length)));
                        client.CurrentPacket = null;
                    }
                }
                else
                {
                    //Console.WriteLine("Client [{0}]: Got Packet", client.WebSocket.RemoteEndPoint);
                    byte zero = client.WriteBuffer[0];
                    byte one = client.WriteBuffer[1];
                    bool fin = (zero & 0x80) == 0x80;
                    //bool rsv1 = (zero & 0x40) == 0x40;
                    //bool rsv2 = (zero & 0x20) == 0x20;
                    //bool rsv3 = (zero & 0x10) == 0x10;
                    byte opCode = (byte)((zero & 0x8) | (zero & 0x4) | (zero & 0x2) | (zero & 0x1));

                    //Console.WriteLine("Client [{0}]: Packet opcode is '0x{1}'", client.WebSocket.RemoteEndPoint, opCode.ToString("X"));
                    switch (opCode)
                    {
                        case 0x08:
                            {
                                Console.WriteLine("Client [{0}]: Closing Connection (browser sent 0x08)", client.WebSocket.RemoteEndPoint);
                                client.Close();
                                return;
                            }
                    }

                    bool mask = (one & 0x80) == 0x80;
                    byte payload = (byte)((one & 0x40) | (one & 0x20) | (one & 0x10) | (one & 0x8) | (one & 0x4) | (one & 0x2) | (one & 0x1));
                    int length = 0;
                    int s = 0;

                    switch (payload)
                    {
                        case 126:
                            length = (int)((client.WriteBuffer[2] << 8) | client.WriteBuffer[3]);
                            s = 2;
                            break;
                        case 127:
                            Console.WriteLine("Client [{0}]: Got Really Large (8-byte length) Packet (not implemented), so I'll just dispose", client.WebSocket.RemoteEndPoint);
                            client.Close();
                            //client.WebSocket.Close();
                            return;

                        default:
                            length = payload;
                            break;
                    }

                    // append data

                    //byte[] maskKeys = null;
                    client.CurrentPacket = new WebSocketClient.Packet();
                    if (mask)
                    {
                        client.CurrentPacket.Mask = new byte[] { client.WriteBuffer[s + 2], client.WriteBuffer[s + 3], client.WriteBuffer[s + 4], client.WriteBuffer[s + 5] };
                        s += 6;
                    }
                    //Console.WriteLine(client.WriteBuffer[0].ToString("x"));

                    client.CurrentPacket.Data = new byte[length];
                    client.CurrentPacket.BytesRead += read - s;
                    //client.CurrentPacket.Data = new byte[length];

                    Array.Copy(client.WriteBuffer, s, client.CurrentPacket.Data, 0, Math.Min(client.CurrentPacket.BytesRead, client.CurrentPacket.Data.Length));
                    if (mask)
                        for (int i = 0; i < client.CurrentPacket.Data.Length; i++)
                            client.CurrentPacket.Data[i] ^= client.CurrentPacket.Mask[i % 4];
                    if (client.CurrentPacket.BytesRead == client.CurrentPacket.Data.Length)
                    {
                        //Console.WriteLine("Match^2: {0} <{1}> {2}", client.CurrentPacket.Data.Length, client.CurrentPacket.BytesRead, read);
                        OnReceiveFromWebSocket(client, client.CurrentPacket.Data, client.CurrentPacket.Data.Length);
                        //Send(client, ASCIIEncoding.UTF8.GetBytes(string.Format("small packet done: thanks for {0} bytes!", client.CurrentPacket.Data.Length)));
                        //Console.WriteLine("got data: {0} '{1}'", length, Encoding.ASCII.GetString(client.CurrentPacket.Data, 0, client.CurrentPacket.Data.Length));
                        client.CurrentPacket = null;
                    }
                }
                if (webSocket != null && webSocket.Connected)
                    webSocket.BeginReceive(client.WriteBuffer, 0, client.MaxBufferSize, SocketFlags.None, new AsyncCallback(ReadCallback), client);
            }
            else
            {
                //Console.WriteLine("Client [d/c]: Disconnection due to invalid state");
                Console.WriteLine("Client [d/c]: No data [1]");
                //client.Close();
                //client.WebSocket.Disconnect(true);
            }
        }

        public static void PrintHexArray(byte[] data, int length)
        {

            //byte[] data = UTF8Encoding.Convert(Encoding.UTF8, Encoding.ASCII, data2, 0, length);
            Console.WriteLine("Packet: {0}, Length: {1}", data[0].ToString("X"), length);
            for (int i = 0; i < data.Length; i++)
            {
                Console.Write("{0:X2} ", data[i]);
                if ((i + 1) % 8 == 0)
                    Console.WriteLine();
            }
            Console.WriteLine();


        }
        public static void OnReceiveFromWebSocket(WebSocketClient client, byte[] data, int length)
        {
            // check if old: uosock == null?
            try
            {
                //if (length == -1)
                    //length = _data.Length;
                // this is fucking dumb
                //char[] cdata = UTF8Encoding.UTF8.GetString(_data, 0, length).ToCharArray();
                //Console.WriteLine("`{0}`", new string(cdata));
                //if (cdata.Length < 4 || cdata.Length % 4 != 0)
                //    return;
                //byte[] data = Convert.FromBase64CharArray(cdata, 0, cdata.Length);

                //Console.WriteLine(.UTF8.GetString(data));

                //Send(client, "da fuqqqq");
                //return;
                    
                //PrintHexArray(data, data.Length);
                //ConvertFromUTF8(ref data, data.Length);

                switch ((char)data[0])
                {
                    case 'R':
                        {
                            if (client.UOSocket != null && client.UOSocket.Connected)
                                client.UOSocket.Close();
                            //client.Compressed = true;
                            goto case 'C';
                        }
                    case 'C':
                        {
                            string[] strData = Encoding.ASCII.GetString(data, 0, data.Length).Split(' ');
                            for (int i = 0; i < strData.Length; i++)
                                Console.Write(strData[i] + ",");
                            client.UOSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            Console.WriteLine(string.Join(",", strData));
                            client.UOSocket.BeginConnect(strData[1], int.Parse(strData[2]), new AsyncCallback(UOConnectCallback), client);
                            break;
                        }
                    case 'V':
                        {
                            Send(client, "Version {0}", Title);
                            break;
                        }
                    default:
                        {
                            //client.UOSocket.Send(data, length, 0);
                            client.UOSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(UOSendCallback), client);
                            break;
                        }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Client [d/c]: Threw {0}... closing!", e.GetType());
                Console.WriteLine(e.StackTrace);
                client.Close();
            }
            // Send(client, Encoding.ASCII.GetBytes("Hello! The length is:" + data.Length));
        }

        public static void UOSendCallback(IAsyncResult ar)
        {
            
            WebSocketClient client = (WebSocketClient)ar.AsyncState;
            
            if (client.UOSocket != null && client.UOSocket.Connected)
                client.UOSocket.EndSend(ar);
        }
        public static void UOReceiveCallback(IAsyncResult ar)
        {
            WebSocketClient client = (WebSocketClient)ar.AsyncState;
            int rec;
            try
            {
                rec = (client.UOSocket != null && client.UOSocket.Connected) ? client.UOSocket.EndReceive(ar) : 0;
            }
            catch (Exception e)
            {
                rec = 0;
            }
            if (rec > 0)
            {
                byte[] buffer = new byte[rec];
                Array.Copy(client.UOWriteBuffer, buffer, rec);

                /*if (client.Compressed) 
                    Huffman.Decompress(client, buffer);
                else*/
                
                    //byte[] buff = new byte[rec];
                    //Array.Copy(client.UOWriteBuffer, buff, rec);
                    Send(client, buffer, PacketType.GamePacket, false);
                
                //Send(client, Convert.ToBase64String(buff));
                if (client.UOSocket != null && client.UOSocket.Connected)
                    client.UOSocket.BeginReceive(client.UOWriteBuffer, 0, client.UOWriteBuffer.Length, SocketFlags.None, new AsyncCallback(UOReceiveCallback), client);
            }
            else
                if (client.UOSocket != null && client.UOSocket.Connected)
                {
                    Console.WriteLine("Client [{0}]: No data", client.WebSocket.RemoteEndPoint);
                    Send(client, "Discon");
                    client.Close();
                }
            //else close?
        }
        public static void UOConnectCallback(IAsyncResult ar)
        {
            WebSocketClient client = (WebSocketClient)ar.AsyncState;
            try
            {
                client.UOSocket.EndConnect(ar);
                Send(client, "ConSuccess", client.UOSocket.RemoteEndPoint);
                client.UOSocket.BeginReceive(client.UOWriteBuffer, 0, client.UOWriteBuffer.Length, SocketFlags.None, new AsyncCallback(UOReceiveCallback), client);
            }
            catch (Exception e)
            {
                Send(client, "ConFail");
            }
            //Send(client, Encoding.ASCII.GetBytes("L Connected: " + client.UOSocket.RemoteEndPoint
        }

        public struct TileInfo
        {
            public bool IsLand;
            public int ID, Z, Hue;

            public TileInfo(bool land, int id, int z, int hue)
            {
                IsLand = land;
                ID = id;
                Z = z;
                Hue = hue;
            }
        }
        
        public static Dictionary<int, byte[]> m_Widths;
        public static byte[] ParseWebRequest(WebSocketClient client, string shortUri, string fullUri)
        {
            try
            {
                switch (shortUri)
                {
                    case "/td":
                        {
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            Dictionary<string, string> tdict = new Dictionary<string, string>();
                            int id = int.Parse(query["i"]);
                            ItemData data = TileData.ItemTable[id];
                            
                            tdict.Add("animation", data.Animation.ToString());
                            tdict.Add("quality", data.Quality.ToString());
                            tdict.Add("quantity", data.Quantity.ToString());
                            tdict.Add("value", data.Value.ToString());
                            tdict.Add("weight", data.Weight.ToString());
                            tdict.Add("flags", "\"" + data.Flags.ToString() + "\"");

                            string formatted = "{\"id\":" + id;
                            foreach (KeyValuePair<string, string> kvp in tdict)
                                formatted += string.Format(", \"{0}\": {1}", kvp.Key, kvp.Value);
                            return Encoding.ASCII.GetBytes(formatted + "}");
                        }
                    case "/getmapinfo":
                        {
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            Dictionary<string, string> tdict = new Dictionary<string, string>();
                            int x = int.Parse(query["x"]);
                            int y = int.Parse(query["y"]);
                            int range = int.Parse(query["r"]);
                            char facet = query["m"][0];
                            //string callback = query["jsoncallback"];
                            
                            //TileInfo[,] info = new TileInfo[range, range];
                            //List<TileInfo>[,] info = new List<TileInfo>[range, range];
                            Map map;
                            switch (facet)
                            {
                                default:
                                case 'f': map = Map.Felucca; break;
                                case 't': map = Map.Trammel; break;
                                case 'i': map = Map.Ilshenar; break;
                                case 'm': map = Map.Malas; break;
                                case 'o': map = Map.Tokuno; break;
                            }
                            
                            string ret = "{";

                            for (int i = 0; i < range; i++)
                            {
                                ret += string.Format("\"{0}\":{{", i + x);
                                for (int j = 0; j < range; j++)
                                {
                                    //ret += j + ": {";

                                    ret += string.Format("\"{0}\":{{", j + y);
                                    //Tile[] land = map.Tiles.GetLandBlock(x + i, y + j);
                                    //foreach (Tile t in land)
                                    //info[x + i, y + j].Add(new TileInfo(true, t.ID, t.Z));
                                    //HuedTile[] s = map.Tiles.GetStaticBlock(x+i, y+j);
                                    int nX = (x + i), nY = (y + j);
                                    
                                    Tile land = map.Tiles.GetLandTile(nX, nY);
                                    
                                    ret += string.Format("\"land\":{{\"id\":{0},\"z\":{1}}}", land.ID, land.Z);
                                    //info[i, j].Add(new TileInfo(true, land.ID, land.Z, 0));
                                    HuedTile[] s = map.Tiles.GetStaticTiles(nX, nY);
                                    Array.Sort<HuedTile>(s);
                                    ret += ", \"length\": " + s.Length;
                                    //foreach (HuedTile tile in s)
                                    //for (int k = s.Length-1; k >= 0; k--)
                                    
                                    //for(int k = 0; k < s.Length; k++)
                                    for (int k = 0; k < s.Length; k++)
                                    {
                                        ret += string.Format(",\"{0}\":{{\"id\":{1},\"z\":{2},\"hue\":{3}}}", k, s[k].ID % 0x4000, s[k].Z, s[k].Hue);
                                        //info[i, j].Add(new TileInfo(false, tile.ID, tile.Z, tile.Hue));
                                    }
                                    ret += "}" + ((j == range - 1) ? "" : ",");
                                }
                                ret += "}" + ((i == range - 1) ? "" : ",");
                            }
                            ret += "}";


                            //Tile[] land = map.Tiles.GetLandBlock(x, y);
                            return ASCIIEncoding.ASCII.GetBytes(ret);
                        }
                    case "/getgump":
                        {
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            int id = int.Parse(query["i"]);
                            int hueIdx = int.Parse(query["h"]);
                            Hue hue = hueIdx == 0 ? null : Hues.List[(hueIdx & 0x3FFF) - 1];

                            Bitmap b = (Bitmap)(Gumps.GetGump(id).Clone());
                            if (hue != null)
                                hue.ApplyTo(b, true);

                            MemoryStream ms = new MemoryStream();
                            b.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                            return ms.GetBuffer();
                        }
                    case "/getobj":
                        {
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            char type = query["t"][0];
                            int id = int.Parse(query["i"]);
                            int hueIdx = int.Parse(query["h"]);
                            string crop = query["c"];
                            Bitmap b = (Bitmap)(type == 'l' ? Art.GetLand(id) : Art.GetStatic(id)); //don't hue the cached, clone it
                            if (b == null)
                                b = Art.GetLand(0);
                            b = (Bitmap)b.Clone();
                            Hue hue = hueIdx == 0 ? null : Hues.List[(hueIdx & 0x3FFF) - 1];
                            if (hue != null)
                                hue.ApplyTo(b, type == 'l');

                            switch (crop)
                            {
                                case "right":
                                    {
                                        for (int x = 0; x < b.Width/2; x++)
                                        {
                                            for (int y = 0; y < b.Height; y++)
                                            {
                                                b.SetPixel(x, y, Color.Transparent);
                                            }
                                        }
                                        break;
                                    }
                                case "left":
                                    {
                                        for (int x = b.Width/2; x < b.Width; x++)
                                        {
                                            for (int y = 0; y < b.Height; y++)
                                            {
                                                b.SetPixel(x, y, Color.Transparent);
                                            }
                                        }
                                        break;
                                    }
                            }
                            MemoryStream ms = new MemoryStream();
                            b.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            return ms.GetBuffer();
                        }
                    case "/getcliloc":
                        {
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            int message = int.Parse(query["i"]);
                            //string callback = query["jsoncallback"];

                            string entry = ((string)StringList.EnglishStringList.Table[message]);
                            int i = 0;
                            string replace = Regex.Replace(entry, @"~[A-Za-z0-9_]+~",  match => query["" + (i++)]);
                            
                            return UTF8Encoding.UTF8.GetBytes("{\"text\":\"" + replace.Replace("\"", "\\\"") + "\"}");

                        }
                    case "/getaniminfo":
                        {
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            int bodyId = int.Parse(query["i"]);
                            int action = int.Parse(query["a"]);
                            int dir = int.Parse(query["d"]);
                            //string callback = query["jsoncallback"];
                            int hash = (bodyId << 16) | (action << 8) | (dir);
                            byte[] widths = null;
                            if (m_Widths.ContainsKey(hash))
                                widths = m_Widths[hash];
                            else
                            {
                                Frame[] frames = Animations.GetAnimation(bodyId, action, dir, 0, true);
                                
                                if (frames == null)
                                    widths = new byte[0];
                                else
                                {
                                    widths = new byte[frames.Length];
                                    for (int i = 0; i < frames.Length; i++)
                                        widths[i] = (byte)frames[i].Bitmap.Width;
                                }
                            }
                            
                            return ASCIIEncoding.ASCII.GetBytes("{\"widths\": [" + string.Join(",", Array.ConvertAll(widths, x => x.ToString())) + "]}");

                        }
                    case "/getanim":
                        {
                            //todo: check if wearable and adjust bitmap accordingly if human
                            NameValueCollection query = HttpUtility.ParseQueryString(fullUri);
                            int bodyId = int.Parse(query["i"]);
                            int action = int.Parse(query["a"]);
                            int dir = int.Parse(query["d"]);
                            int hueIdx = int.Parse(query["h"]);
                            Frame[] frames = Animations.GetAnimation(bodyId, action, dir, 0, true);
                            Hue hue = hueIdx == 0 ? null : Hues.List[(hueIdx & 0x3FFF) - 1];
                            int hash = (bodyId << 16) | (action << 8) | (dir);
                            if (frames == null)
                                return new byte[] { };

                            int maxWidth = 0, maxHeight = 0;
                            foreach (Frame frame in frames)
                            {
                                if (frame.Bitmap.Width > maxWidth)
                                    maxWidth = frame.Bitmap.Width; // +Math.Abs(frame.Center.X);
                                if (frame.Bitmap.Height > maxHeight)
                                    maxHeight = frame.Bitmap.Height; // +Math.Abs(frame.Center.Y);
                            }
                            // should we cache full animation bitmaps?
                            Bitmap bitmap = new Bitmap(maxWidth * frames.Length, maxHeight);
                            Graphics g = Graphics.FromImage(bitmap);
                            
                            byte[] widths = new byte[frames.Length];
                            for (int i = 0; i < frames.Length; i++)
                            {
                                Frame frame = frames[i];
                                Bitmap single = frame.Bitmap;
                                widths[i] = (byte)single.Width;
                                g.DrawImageUnscaled(single, i * maxWidth, 0);
                            }
                            if(!m_Widths.ContainsKey(hash))
                            m_Widths.Add(hash, widths);
                            if (hueIdx != 0)
                                hue.ApplyTo(bitmap, TileData.AnimationData.ContainsKey(bodyId) && (TileData.AnimationData[bodyId].Flags & TileFlag.PartialHue) != 0);
                            bitmap.SetPixel(0, 0, Color.FromArgb(0, 0, 0, frames.Length));
                            MemoryStream ms = new MemoryStream();
                            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            return ms.GetBuffer();
                        }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0} {1}", e.Message, e.StackTrace);
                return ASCIIEncoding.ASCII.GetBytes(string.Format("An error has occurred: {0} {1}", e.Message, e.StackTrace));
            }
            return new byte[] { };
        }

        #region Sending
        public static void Send(WebSocketClient client, string format, params object[] o)
        {
            Send(client, Encoding.UTF8.GetBytes(string.Format(format, o)), PacketType.ClientPacket, false);
        }

        public static void Send(WebSocketClient client, byte[] data, PacketType type, bool mask)
        {
            // note to self: do not mask anything :|
            //PrintHexArray(data, data.Length);
            int headerLength = 2;
            byte payload = 0;
            byte[] maskKeys = null;
            // ??? resize data & preappend a type ident

            byte[] tmp = new byte[data.Length + 1];
            tmp[0] = (byte)type;
            Array.Copy(data, 0, tmp, 1, data.Length);
            data = tmp;
            
            //data = Encoding.UTF8.GetBytes(((char)type) + Convert.ToBase64String(data));

            /*
             * because browsers don't support receiving directly into an array, it has to be a proper utf8 string instead :|
             * this seems super dumb to do, but it works (if it's not there, both ff and chrome won't receive data).
             */
            //data = Encoding.UTF8.GetBytes(Encoding.ASCII.GetString(data));
            //Console.WriteLine("Raw length={0}", data.Length);
            if (data.Length > short.MaxValue)
                Console.WriteLine("Client [{0}]: Sending Really Large Packet (not implemented)", client.WebSocket.RemoteEndPoint);
            if (data.Length >= 126)
            {
                headerLength += 2;
                payload = 126;
            }
            else
                payload = (byte)data.Length;
            if (mask)
            {
                headerLength += 4;
                Random r = new Random();
                maskKeys = new byte[] { 1, 2, 3, 4 };
                r.NextBytes(maskKeys);
            }

            //int clientHeaderOffset = type == PacketType.None ? 0 : 1;
            byte[] allData = new byte[headerLength + data.Length];
            allData[0] = 0x80 | 0x02;
            allData[1] = (byte)((mask ? 0x80 : 0) | payload & 0x40 | payload & 0x20 | payload & 0x10 | payload & 0x8 | payload & 0x4 | payload & 0x2 | payload & 0x1);
            //allData[0] &= 0x02;
            //allData[1] = (byte)(allData[1] & ~((byte)1));

            if (payload == 126)
            {
                byte[] lengthBytes = BitConverter.GetBytes((ushort)data.Length);
                allData[2] = lengthBytes[1]; // (byte)((data.Length >> 8) & 0xFF);
                allData[3] = lengthBytes[0]; // (byte)(data.Length & 0xFF);
            }

            Array.Copy(data, 0, allData, headerLength, data.Length);
            if (mask)
            {
                Array.Copy(maskKeys, 0, allData, 2, 4);
                for (int i = 0; i < data.Length; i++)
                    allData[i + headerLength] ^= maskKeys[i % 4];
            }

            //PrintHexArray(allData, allData.Length);
            //allData = Encoding.UTF8.GetBytes();
            //Console.WriteLine(Encoding.UTF8.GetSting(allData));

            if (client.WebSocket != null && client.WebSocket.Connected)
            {
                //client.WebSocket.Send(allData);
                client.WebSocket.BeginSend(allData, 0, allData.Length, SocketFlags.None, new AsyncCallback(SendCallback), client);
            }
        }
        public static void WebSendCallback(IAsyncResult ar)
        {
            WebSocketClient client = (WebSocketClient)ar.AsyncState;
            if (client.WebSocket != null && client.WebSocket.Connected)
            {
                Console.WriteLine("Client [{0}]: Sent {1} bytes", client.WebSocket.RemoteEndPoint, client.WebSocket != null && client.WebSocket.Connected ? client.WebSocket.EndSend(ar) : 0);
                client.Close();
            }
        }
        public static void SendCallback(IAsyncResult ar)
        {
            WebSocketClient client = (WebSocketClient)ar.AsyncState;
            if (client.WebSocket != null && client.WebSocket.Connected)
            {
                Console.WriteLine("Client [{0}]: Sent {1} bytes", client.WebSocket.RemoteEndPoint, client.WebSocket != null && client.WebSocket.Connected ? client.WebSocket.EndSend(ar) : 0);
            }
        }
        #endregion
    }
}